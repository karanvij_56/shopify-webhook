const express = require('express');
const logger = require('../logger');
const router = express.Router();

router.post('/', (req, res) => {
    const data = req.body;
    console.log('WEBHOOK Run');
    console.log('WEBHOOK DATA',data);
    logger.info({Webhook_data:data});
    return res.send(data);
});


module.exports = router;